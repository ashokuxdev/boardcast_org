import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FemaleArtistComponent } from './female-artist/female-artist.component';
import { FilterListComponent } from './filter-list/filter-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatListModule} from '@angular/material';
//import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import { PaginatorComponent } from './paginator/paginator.component';
import { MaterialModule } from "../material/material.module";
//import {MatListModule} from '@angular/material/list';


@NgModule({
  declarations: [
    FemaleArtistComponent,
    FilterListComponent,
    PaginatorComponent
  ],
  imports: [
    CommonModule,
    //MatToolbarModule,
    //MatButtonModule,
    //MatCheckboxModule,
    //MatListModule,
    //MatCardModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
   // MatCheckboxModule
    
  ]
})
export class ArtistsModule { }
