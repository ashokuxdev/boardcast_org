import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../shared/employee.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private service: EmployeeService) { }

  departments = [
    { id: 3, value: 'Dep 1' },
    { id: 2, value: 'Dep 2' },
    { id: 3, value: 'Dep 3' }
  ];

  interests = [
    { value: 'acting-0', viewValue: 'Acting' },
    { value: 'singing-1', viewValue: 'Singing' },
    { value: 'writting-2', viewValue: 'Writting' }
  ];

  countries = [
    { value: 'india-0', viewValue: 'India' },
    { value: 'usa-1', viewValue: 'USA' },
    { value: 'canada-2', viewValue: 'Canada' }
  ];

  genders = [
    { value: 'men-0', viewValue: 'Men' },
    { value: 'women-1', viewValue: 'Women' }
  ];

  public variables = ['One','Two','County', 'Three', 'Zebra', 'XiOn'];
    

    public filteredList1 = this.variables.slice();
    

  ngOnInit() {
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  }

}
