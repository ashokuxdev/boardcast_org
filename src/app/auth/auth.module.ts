import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthComponent } from './auth.component';
import { NoAuthGuard } from './no-auth-guard.service';
import { SharedModule } from '../shared';
import { AuthRoutingModule } from './auth-routing.module';
import { SignupComponent } from './signup/signup.component';
import { MaterialModule } from "../material/material.module";
import { EmployeeService } from '../shared/employee.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  imports: [
    SharedModule,
    AuthRoutingModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AuthComponent,
    SignupComponent,
    LoginComponent
  ],
  providers: [
    NoAuthGuard,
    EmployeeService
  ]
})
export class AuthModule {}
