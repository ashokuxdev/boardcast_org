import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from "../material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { HomeComponent } from './home.component';
import { HomeAuthResolver } from './home-auth-resolver.service';
import { SharedModule } from '../shared';
import { HomeRoutingModule } from './home-routing.module';
import { TopStoriesComponent } from './top-stories/top-stories.component';
import { HomeArticlesComponent } from './home-articles/home-articles.component';
import { RightSidebarComponent } from './right-sidebar/right-sidebar.component';

@NgModule({
  imports: [
    SharedModule,
    HomeRoutingModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  declarations: [
    HomeComponent,
    TopStoriesComponent,
    HomeArticlesComponent,
    RightSidebarComponent
  ],
  providers: [
    HomeAuthResolver
  ]
})
export class HomeModule {}
